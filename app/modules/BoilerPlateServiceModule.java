package modules;

import com.google.inject.AbstractModule;
import services.HomeService;
import services.impl.HomeServiceImpl;

public class BoilerPlateServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HomeService.class).to(HomeServiceImpl.class).asEagerSingleton();
    }
}
