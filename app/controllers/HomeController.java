package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import models.Home.HomeRequest;
import play.mvc.*;
import services.HomeService;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    HomeService homeService;

    @Inject
    public HomeController(HomeService homeService){
        this.homeService = homeService;
    }

    public static String getQueryParam(Http.Request request, String param) {
        String[] params = (String[])request.queryString().get(param);
        return params != null && params.length >= 1 ? ((String[])request.queryString().get(param))[0] : null;
    }

    public Result testingController(Http.Request request){
        String log = "POST /boilerplate/test";
        String param = getQueryParam(request, "data");
        String result = homeService.doSomethingToParam(param);
        return ok("Successfully " + log + " with param " + param + "\n result = " + result);
    }

    public Result testingHome(Http.Request request){
        String log = "POST /boilerplate/home";
        try {
            HomeRequest homeRequest = getHomeRequest(request);
            String message = homeService.doSomethingToHomeRequest(homeRequest);
            return ok("Successfully " + log + "\n message = " + message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return badRequest();
        }
    }

    private HomeRequest getHomeRequest(Http.Request request) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(request.body().asJson().toString(), HomeRequest.class);
    }
}
