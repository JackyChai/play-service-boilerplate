package services;

import models.Home.HomeRequest;

public interface HomeService {

    String doSomethingToParam(String param);

    String doSomethingToHomeRequest(HomeRequest homeRequest);
}
