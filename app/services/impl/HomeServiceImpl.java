package services.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import models.Home.HomeRequest;
import org.springframework.util.StringUtils;
import services.HomeService;

@Singleton
public class HomeServiceImpl implements HomeService {

    @Inject
    public HomeServiceImpl(){

    }

    @Override
    public String doSomethingToParam(String param) {
        String data = "";
        if(!StringUtils.isEmpty(param)){
             data = "We received your data as: " + param + ", thank you.";
        }
        System.out.println(data);
        return data;
    }

    @Override
    public String doSomethingToHomeRequest(HomeRequest homeRequest) {
        return "Your house at: " + homeRequest.getHouse() + ", " + homeRequest.getStreet() + ", " + homeRequest.getState();
    }
}
